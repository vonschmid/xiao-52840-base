// Copyright (c) 2023 Nordic Semiconductor ASA
// SPDX-License-Identifier: Apache-2.0

/dts-v1/;
#include <nordic/nrf52840_qiaa.dtsi>

/ {
	model = "xiao-expansion";
	compatible = "seed,xiao-expansion";

	chosen {
		zephyr,sram = &sram0;
		zephyr,flash = &flash0;
		zephyr,code-partition = &slot0_partition;
		zephyr,display = &ssd1306;
	};

	aliases {
		led0 = &led0;
		led1 = &led1;
		led2 = &led2;
		sw0 = &button0;
		buzzer-pwm = &buzzer;
	};

	buttons {
		compatible = "gpio-keys";
		button0: button_0 { 
			gpios = <&gpio0 3 (GPIO_PULL_UP | GPIO_ACTIVE_LOW)>;
			label = "Push button switch 0";
		};
	};

	leds {
		compatible = "gpio-leds";
		led0: led_0 {
			gpios = <&gpio0 26 GPIO_ACTIVE_LOW>;
			label = "Green LED 0";
		};
		led1: led_1 {
			gpios = <&gpio0 30 GPIO_ACTIVE_LOW>;
			label = "Green LED 1";
		};
		led2: led_2 {
			gpios = <&gpio0 6 GPIO_ACTIVE_LOW>;
			label = "Green LED 2";
		};
	};

	pmwbuzzer {
		compatible = "pwm-leds";
		status = "okay";

		buzzer: buzzer_pwm{
			pwms = < &pwm1 0 PWM_HZ(880) PWM_POLARITY_NORMAL>;
			label = "PMW_1";
		};
	};
};

&gpio0 {
	status = "okay";
};

&flash0 {
	partitions {
		compatible = "fixed-partitions";
		#address-cells = <1>;
		#size-cells = <1>;

		boot_partition: partition@0 {
			label = "mcuboot";
			reg = <0x0 0xc000>;
		};
		slot0_partition: partition@c000 {
			label = "image-0";
			reg = <0xc000 0x72000>;
		};
		slot1_partition: partition@7e000 {
			label = "image-1";
			reg = <0x7e000 0x72000>;
		};
		scratch_partition: partition@f0000 {
			label = "image-scratch";
			reg = <0xf0000 0xa000>;
		};
		storage_partition: partition@fa000 {
			label = "storage";
			reg = <0xfa000 0x6000>;
		};
	};
};

&i2c1 {
	status = "okay";
    compatible = "nordic,nrf-twi";
	status = "okay";
	clock-frequency = <I2C_BITRATE_FAST>;
	pinctrl-0 = <&i2c1_default>;
	pinctrl-1 = <&i2c1_sleep>;
	pinctrl-names = "default", "sleep";

	ssd1306: ssd1306@3c {
		compatible = "solomon,ssd1306fb";
		reg = <0x3c>;
		label = "SSD1306";
		width = <128>;
		height = <64>;
		segment-offset = <0>;
		page-offset = <0>;
		display-offset = <0>;
		multiplex-ratio = <63>;
		segment-remap;
		com-invdir;
		prechargep = <0x22>;
	};
};

&pwm1 {
	status = "okay";
	pinctrl-0 = <&pwm1_default>;
	pinctrl-1 = <&pwm1_sleep>;
	pinctrl-names = "default", "sleep";
};

&pinctrl {
	pwm1_default: pwm1_default {
		group1 {
			psels = <NRF_PSEL(PWM_OUT0, 0, 29)>;
		};
	};

	pwm1_sleep: pwm1_sleep {
		group1 {
			psels = <NRF_PSEL(PWM_OUT0, 0, 29)>;
			low-power-enable;
		};
	};

	i2c1_default: i2c1_default {
		group1 {
			psels = <NRF_PSEL(TWIM_SDA, 0, 4)>,
				<NRF_PSEL(TWIM_SCL, 0, 5)>;
		};
	};

	i2c1_sleep: i2c1_sleep {
		group1 {
			psels = <NRF_PSEL(TWIM_SDA, 0, 4)>,
				<NRF_PSEL(TWIM_SCL, 0, 5)>;
			low-power-enable;
		};
	};
};
