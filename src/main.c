#include <zephyr/types.h>
#include <zephyr/kernel.h>
#include <dk_buttons_and_leds.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/display.h>
#include <zephyr/drivers/pwm.h>
#include <zephyr/logging/log.h>
#include <zephyr/random/rand32.h>

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/hci.h>
#include <bluetooth/services/nus.h>

#include <lvgl.h>

LOG_MODULE_REGISTER(bomb, 4);

#define LED_RED DK_LED1
#define LED_GREEN DK_LED2
#define LED_BLUE DK_LED3

#define WORQ_THREAD_STACK_SIZE 8196

#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME) - 1)


static const struct bt_data ad[] = {
    BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
    BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN),
};

static const struct bt_data sd[] = {
    BT_DATA_BYTES(BT_DATA_UUID128_ALL, BT_UUID_NUS_VAL),
};

static struct k_work_q display_work_q = {0};
static K_THREAD_STACK_DEFINE(display_stack_area, WORQ_THREAD_STACK_SIZE);
struct k_work display_work;

const struct pwm_dt_spec buzzer = PWM_DT_SPEC_GET(DT_ALIAS(buzzer_pwm));

static struct device *display_dev;
static lv_obj_t *lv_label_1;

void show()
{
    lv_task_handler();
}

void command_handler(uint8_t command)
{
    LOG_INF("command received: %x", command);
}

static void connected(struct bt_conn *conn, uint8_t err)
{
    char addr[BT_ADDR_LE_STR_LEN];

    if (err)
    {
        LOG_ERR("Connection failed (err %u)", err);
        return;
    }

    bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));
    LOG_INF("Connected %s", addr);
}

static void disconnected(struct bt_conn *conn, uint8_t reason)
{
    char addr[BT_ADDR_LE_STR_LEN];

    bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

    LOG_INF("Disconnected: %s (reason %u)", addr, reason);
}

BT_CONN_CB_DEFINE(conn_callbacks) = {
    .connected = connected,
    .disconnected = disconnected,
};

static void bt_receive_cb(struct bt_conn *conn, const uint8_t *const data,
                          uint16_t len)
{
    char addr[BT_ADDR_LE_STR_LEN] = {0};

    bt_addr_le_to_str(bt_conn_get_dst(conn), addr, ARRAY_SIZE(addr));

    LOG_INF("Received data from: %s", addr);

    command_handler(data[0]);
}

static struct bt_nus_cb nus_cb = {
    .received = bt_receive_cb,
};

void button_changed(uint32_t button_state, uint32_t has_changed)
{
    uint32_t buttons = button_state & has_changed;

    if (buttons)
    {
        dk_set_led_on(LED_BLUE);
        pwm_set_dt(&buzzer, PWM_HZ(3000), PWM_HZ(3000) / 2);
        k_msleep(100);
        pwm_set_dt(&buzzer, 0, 0);
        dk_set_led_off(LED_BLUE);
    }
}

int main(void)
{
    LOG_INF("started");
    int err;

    lv_init();

    display_dev = DEVICE_DT_GET(DT_CHOSEN(zephyr_display));
    if (!device_is_ready(display_dev))
    {
        LOG_ERR("Device not ready, aborting test");
        dk_set_led_on(LED_RED);
        return -1;
    }

    err = dk_leds_init();
    if (err)
    {
        LOG_ERR("Cannot init LEDs (err: %d)", err);
        dk_set_led_on(LED_RED);
        return -1;
    }

    lv_label_1 = lv_label_create(lv_scr_act());

    lv_obj_align(lv_label_1, LV_ALIGN_TOP_MID, 0, 0);

    lv_label_set_text(lv_label_1, "hello");

    err = dk_buttons_init(button_changed);
    if (err)
    {
        LOG_ERR("Cannot init buttons (err: %d)", err);
        dk_set_led_on(LED_RED);
        return -1;
    }

    k_work_queue_start(&display_work_q, display_stack_area,
                       K_THREAD_STACK_SIZEOF(display_stack_area), 5,
                       NULL);

    k_work_init(&display_work, show);

    err = bt_enable(NULL);
    if (err)
    {
        LOG_ERR("Cannot init BLE (err: %d)", err);
        dk_set_led_on(LED_RED);
        return -1;
    }

    err = bt_nus_init(&nus_cb);
    if (err)
    {
        LOG_ERR("Failed to initialize UART service (err: %d)", err);
        dk_set_led_on(LED_RED);
        return -1;
    }

    err = bt_le_adv_start(BT_LE_ADV_CONN, ad, ARRAY_SIZE(ad), sd,
                          ARRAY_SIZE(sd));
    if (err)
    {
        LOG_ERR("Advertising failed to start (err %d)", err);
        dk_set_led_on(LED_RED);
        return -1;
    }

    dk_set_led_on(LED_GREEN);
    pwm_set_dt(&buzzer, PWM_HZ(1000), PWM_HZ(1000) / 2);
    k_msleep(100);
    pwm_set_dt(&buzzer, 0, 0);
    dk_set_led_off(LED_GREEN);

    k_work_submit_to_queue(&display_work_q, &display_work);
}